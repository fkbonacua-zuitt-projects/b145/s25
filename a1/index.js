// db.hotel.insertOne({});
db.rooms.insertOne(
		{
			name: "single",
			accomodates: 2,
			price: 1000,
			description: "A simple room with all the basic necessities",
			rooms_available: 10,
			isAvailable: false
		}
	);

// insert multiple rooms
// db.rooms.insertMany({});
db.rooms.insertMany(
	[
		{
		name: "double",
		accomodates: 3,
		price: 2000,
		description: "A fit for a small family going on a vacation",
		rooms_available: 5,
		isAvailable: false
		},
		{
		name: "queen",
		accomodates: 4,
		price: 4000,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_available: 15,
		isAvailable: false
		}
		]
	);
// use the find method to seatch for a room with the name double
db.rooms.find({name: "double"});

// use the updateOne method to update the queen room and set the available rooms to 0

db.rooms.updateOne(
		{rooms_available: 15},
			{
				$set: {
					rooms_available: 0
				}
			}
		);

// use the deleteMany method rooms to delete all rooms that have 0 availability

db.rooms.deleteMany({rooms_available: 0});